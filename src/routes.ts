export enum Routes {
  SEARCH = '/search/movie',
  POPULAR = '/movie/popular',
  TOP_RATED = '/movie/top_rated',
  UPCOMING = '/movie/upcoming',
}