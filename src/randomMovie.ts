export const getRandomMovie = (movies: Array<MovieData>): MovieData => {
    const randomIndex = Math.floor(Math.random() * movies.length);
    return movies[randomIndex];
  };
  
export const getRandomFilmElement = ({ original_title, overview } : MovieData): string => {
  return `
  <div class="row py-lg-5">
  <div
      class="col-lg-6 col-md-8 mx-auto"
      style="background-color: #2525254f"
  >
      <h1 id="random-movie-name" class="fw-light text-light">${original_title}</h1>
      <p id="random-movie-description" class="lead text-white">
          ${overview}
      </p>
  </div>
  </div>`
};