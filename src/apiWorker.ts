import { API_KEY } from "./constants";
import { addLikeListeners } from "./likes";
import { renderResponseData } from "./cardsRenderer";
import { Routes } from "./routes";

const getUrl = (route: Routes, query : string | null, page: number): string => {
  let query_variable = '';
  if (query) {
    query_variable = `&query=${query}`;
  }
  return `http://api.themoviedb.org/3${route}?api_key=${API_KEY}${query_variable}&page=${page}`;
}

export const callApi = (route: Routes, query?: string | null, page = 1) => {
  let searchUrl: string;
  if (route === Routes.SEARCH) {
    if (query) {
      searchUrl = getUrl(route, query, page);
    } else return;
  }
  else searchUrl = getUrl(route, null, page);
  fetch(searchUrl)
    .then(response => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      return response.json();
    })
    .then(data => {
      renderResponseData(data.results);
      addLikeListeners();
  });
};
  