interface MovieData {
  poster_path: string,
  overview: string,
  release_date: string,
  backdrop_path: string,
  original_title: string,
  id: number,
}
