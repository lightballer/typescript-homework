import { POSTER_URL_TEMPLATE } from "./constants";
import { getFilmCardElement } from "./filmCard";
import { LikeColors } from "./likeColors";
import { getRandomFilmElement, getRandomMovie } from "./randomMovie";

export const renderResponseData = (results: Array<MovieData>) => {
    const filmContainer : Element | null = document.getElementById('film-container');
    if (!filmContainer) return;
    for (const item of results) {
      let likeColor = LikeColors.PALE_RED;
      if (localStorage.getItem(item.id.toString())) {
        likeColor = LikeColors.RED;
      };
      filmContainer.appendChild(getFilmCardElement(item, likeColor));
    }
    const randomMovie: MovieData = getRandomMovie(results);
    const randomMovieElement: string = getRandomFilmElement(randomMovie);
    const randomMovieContainer: HTMLElement | null = document.getElementById('random-movie');
    if (randomMovieContainer) {
      randomMovieContainer.innerHTML = randomMovieElement;
      randomMovieContainer.style.backgroundImage = `url(${POSTER_URL_TEMPLATE}${randomMovie.backdrop_path})`;
    }
  };