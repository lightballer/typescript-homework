import { callApi } from './apiWorker';
import { renderFavourites } from './favourites';
import { Routes } from './routes';

const clearFilmContainer = () => {
  const filmContainerElement = document.getElementById('film-container');
  if (filmContainerElement) filmContainerElement.innerHTML = '';
};

export async function render(): Promise<void> {
  // TODO render your app here

  let currentRoute = Routes.POPULAR;
  let currentQuery = '';
  let currentPage = 1;

  callApi(Routes.POPULAR);
  renderFavourites();
  document.getElementById('popular')?.addEventListener('change', event => {
    clearFilmContainer();
    callApi(Routes.POPULAR);
    currentRoute = Routes.POPULAR;
  });

  document.getElementById('upcoming')?.addEventListener('change', event => {
    clearFilmContainer();
    callApi(Routes.UPCOMING);
    currentRoute = Routes.UPCOMING;
  });

  document.getElementById('top_rated')?.addEventListener('change', event => {
    clearFilmContainer();
    callApi(Routes.TOP_RATED);
    currentRoute = Routes.TOP_RATED;
  });

  document.getElementById('submit')?.addEventListener('click', event => {
    clearFilmContainer();
    currentQuery = (<HTMLInputElement>document.getElementById('search')).value;
    callApi(Routes.SEARCH, currentQuery);
    currentRoute = Routes.SEARCH;
  });

  document.getElementById('load-more')?.addEventListener('click', event => {
    callApi(currentRoute, currentQuery, ++currentPage);
  });
}
