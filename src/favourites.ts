import { API_KEY } from "./constants";
import { getFavouriteCardElement } from "./favouriteCard";
import { LikeColors } from "./likeColors";

const dispayFavourites = (data: MovieData) => {
  const favouritesContainer : Element | null = document.getElementById('favorite-movies');
  let likeColor = LikeColors.PALE_RED;
  if (localStorage.getItem(data.id.toString())) {
    likeColor = LikeColors.RED;
  };
  favouritesContainer?.appendChild(getFavouriteCardElement(data));
};

const callApiFavourites = (searchUrl: string) => {
  fetch(searchUrl)
  .then(response => {
    if (!response.ok) {
      throw new Error(response.statusText);
    }
    return response.json();
  })
  .then(data => {
    dispayFavourites(data);
  });
}

export const renderFavourites = () => {
  const favouritesContainer : Element | null = document.getElementById('favorite-movies');
  if (!favouritesContainer) return;
  favouritesContainer.innerHTML = '';
  for (let i = 0; i < localStorage.length; i++) {
    const key: string | null = localStorage.key(i);
    if (!key) return;
    const id = localStorage.getItem(key);
    const searchUrl = `http://api.themoviedb.org/3/movie/${id}?api_key=${API_KEY}`;
    callApiFavourites(searchUrl);
  }
};

export const addFavourite = (idToString: string) => {
  localStorage.setItem(idToString, idToString);
};

export const removeFavourite = (idToString: string) => {
  localStorage.removeItem(idToString);
}
  