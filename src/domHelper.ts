export function createElement(tagName: string, className: string, attributes = {} ): HTMLElement {
  const element = document.createElement(tagName);
  
  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }
  
  Object.keys(attributes).forEach((key: string) => element.setAttribute(key, attributes[key]));
  
  return element;
}
  