import { addFavourite, removeFavourite, renderFavourites } from './favourites';
import { LikeColors } from './likeColors';

export const addLikeListeners = () => {
  const likeButtons = document.querySelectorAll('.bi-heart-fill');
  likeButtons.forEach(button => {
    button.addEventListener('click', event => {
      const color = button.getAttribute('fill');
      let id: string | null = button.getAttribute("id");
      if (!id) return;
      if (color === LikeColors.RED) {
        button.setAttribute('fill', LikeColors.PALE_RED);
        removeFavourite(id);
      }
      else {
        button.setAttribute('fill', LikeColors.RED);
        if (!localStorage.getItem(id)) addFavourite(id);
      }
      renderFavourites();
    });
  });
};